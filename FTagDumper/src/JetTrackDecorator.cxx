#include "JetTrackDecorator.hh"
#include "xAODJet/Jet.h"

#include "InDetTrackSystematicsTools/InDetTrackTruthOriginDefs.h"

// the constructor just builds the decorator
JetTrackDecorator::JetTrackDecorator(const std::string& suffix,
				     const std::string& writer_name)
  : m_ftagTruthOriginLabel("ftagTruthOriginLabel"+suffix),
    m_n_tracks("n_"+writer_name),
    m_PU_track_fraction("PU_fraction_"+writer_name) {}

// this call actually does the work on the jet
void JetTrackDecorator::decorate
(const xAOD::Jet& jet,
 const std::vector<const xAOD::TrackParticle*>& tracks) const {

  int n_PU_tracks = 0;
  for(const auto& track : tracks){
    if(m_ftagTruthOriginLabel.withDefault(*track, -1)==InDet::ExclusiveOrigin::Pileup ||
       m_ftagTruthOriginLabel.withDefault(*track, -1)==InDet::ExclusiveOrigin::Fake)
      n_PU_tracks++;
  }

  m_n_tracks(jet) = tracks.size();

  m_PU_track_fraction(jet) = static_cast<float>(n_PU_tracks)/tracks.size();

}
